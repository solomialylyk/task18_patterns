package solomia.delivery;

import solomia.decorator.BouquetDecorator;

public class DeliveryOrdinary extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 30;
    private  final String TO_NAME = "+ Delivery Ordinary ";

    public DeliveryOrdinary() {
        setToName(TO_NAME);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
