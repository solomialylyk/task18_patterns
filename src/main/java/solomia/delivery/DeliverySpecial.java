package solomia.delivery;

import solomia.decorator.BouquetDecorator;

public class DeliverySpecial extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 60;
    private  final String TO_NAME = "+ Delivery special ";

    public DeliverySpecial() {
        setToName(TO_NAME);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
