package solomia.payment;

import solomia.decorator.BouquetDecorator;

public class ByCreditCard extends BouquetDecorator {
    private  final String TO_NAME = "Payment by credit card ";

    public ByCreditCard() {
        setAdditionalFlowers(TO_NAME);
    }
}
