package solomia.payment;

import solomia.decorator.BouquetDecorator;

public class ByCash extends BouquetDecorator {
    private  final String TO_NAME = "Payment by cash ";

    public ByCash() {
        setAdditionalFlowers(TO_NAME);
    }
}
