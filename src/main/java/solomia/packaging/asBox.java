package solomia.packaging;

import solomia.decorator.BouquetDecorator;

public class asBox extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private final String ADDITIONAL_COMPONENT= "packed as box ";

    public asBox() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setToName(ADDITIONAL_COMPONENT);
    }
}
