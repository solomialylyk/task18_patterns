package solomia.packaging;

import solomia.decorator.BouquetDecorator;

public class withGiftPaper extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private final String ADDITIONAL_COMPONENT= "packed with gift paper ";

    public withGiftPaper() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setToName(ADDITIONAL_COMPONENT);
    }
}
