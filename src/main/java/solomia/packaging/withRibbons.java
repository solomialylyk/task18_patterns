package solomia.packaging;

import solomia.decorator.BouquetDecorator;

public class withRibbons extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private final String ADDITIONAL_COMPONENT= "packed with rabbins ";

    public withRibbons() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setToName(ADDITIONAL_COMPONENT);
    }
}
