package solomia.bouquet.impl;

import solomia.bouquet.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class forFuneral implements Bouquet {
    private double price = 250;
    private String name = "forFuneral ";
    private List<String> flowers;

    public forFuneral() {
        flowers = new LinkedList<>();
        flowers.add("22 carnations");
        this.flowers=flowers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public List<String> getFlowers() {
        return flowers;
    }

}
