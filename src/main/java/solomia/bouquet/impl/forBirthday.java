package solomia.bouquet.impl;

import solomia.bouquet.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class forBirthday implements Bouquet {
    private double price = 350;
    private String name = "forBirthday ";
    private List<String> flowers;

    public forBirthday() {
        flowers= new LinkedList<>();
        flowers.add("5 roses");
        flowers.add("10 daisies");
        this.flowers=flowers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public List<String> getFlowers() {
        return flowers;
    }
}
