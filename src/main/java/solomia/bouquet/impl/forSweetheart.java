package solomia.bouquet.impl;

import solomia.bouquet.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class forSweetheart implements Bouquet {
    private double price = 1000;
    private String name = "forSweetheart ";
    private List<String> flowers;

    public forSweetheart() {
        flowers = new LinkedList<>();
        flowers.add("101 roses");
        this.flowers=flowers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public List<String> getFlowers() {
        return flowers;
    }

}
