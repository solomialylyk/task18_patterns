package solomia.bouquet.impl;

import solomia.bouquet.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class MixBouquet implements Bouquet {
    private double price= 100;
    private String name = "MixBouquet ";
    private List<String> flowers;

    public MixBouquet() {
        flowers = new LinkedList<>();
        this.flowers=flowers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public List<String> getFlowers() {
        return flowers;
    }

}
