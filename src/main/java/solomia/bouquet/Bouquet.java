package solomia.bouquet;

import java.util.List;

public interface Bouquet {
    List<String> getFlowers();

    double getPrice();

    String getName();
}
