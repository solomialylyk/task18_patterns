package solomia.factory.impl;

import solomia.Model;
import solomia.bouquet.Bouquet;
import solomia.bouquet.impl.MixBouquet;
import solomia.bouquet.impl.forBirthday;
import solomia.bouquet.impl.forFuneral;
import solomia.bouquet.impl.forSweetheart;
import solomia.factory.FlowerStore;

public class Store1 extends FlowerStore {
    @Override
    protected Bouquet createBouquet(Model model) {
        Bouquet bouquet= null;
        if (model==Model.forBirthday) {
            bouquet = new forBirthday();
        } else if (model == Model.forFuneral) {
            bouquet = new forFuneral();
        } else if (model == Model.forSweetheart) {
            bouquet = new forSweetheart();
        } else if (model == Model.mixBouquet) {
            bouquet = new MixBouquet();
        }
        return bouquet;
    }
}
