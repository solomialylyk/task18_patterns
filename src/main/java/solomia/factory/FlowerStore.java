package solomia.factory;

import solomia.Model;
import solomia.bouquet.Bouquet;

public abstract class FlowerStore  {
    protected abstract Bouquet createBouquet(Model model);

    public Bouquet assemble(Model model) {
        Bouquet bouquet = createBouquet(model);
        return bouquet;
    }

}
