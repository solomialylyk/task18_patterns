package solomia.sorts;

import solomia.decorator.BouquetDecorator;

public class Tulip extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private  final String ADDITIONAL_FLOWER= "1 tulip";

    public Tulip() {
        setAdditionalFlowers(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
