package solomia.sorts;

import solomia.decorator.BouquetDecorator;

public class Daisy extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private  final String ADDITIONAL_FLOWER= "1 daisy";

    public Daisy() {
        setAdditionalFlowers(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
