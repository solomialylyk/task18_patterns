package solomia.sorts;

import solomia.decorator.BouquetDecorator;

public class Rose extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private  final String ADDITIONAL_FLOWER= "1 rose";

    public Rose() {
        setAdditionalFlowers(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
