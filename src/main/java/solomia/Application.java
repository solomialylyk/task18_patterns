package solomia;

import solomia.bouquet.Bouquet;
import solomia.decorator.BouquetDecorator;
import solomia.packaging.*;
import solomia.factory.FlowerStore;
import solomia.factory.impl.Store2;
import solomia.sorts.*;
import solomia.delivery.*;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        FlowerStore flowerStore = new Store2();
        Bouquet bouquet1 = flowerStore.assemble(Model.forBirthday);
        System.out.println("Price : " + bouquet1.getPrice());
        System.out.println("Name : " + bouquet1.getName());

        BouquetDecorator bouquetDecoratorTulip = new Tulip();
        BouquetDecorator bouquetDecoratorTulip1 = new Tulip();
        BouquetDecorator bouquetDecoratorPackaging = new asBox();
        BouquetDecorator bouquetDecoratorDelivery = new DeliveryOrdinary();

        bouquetDecoratorTulip.setBouquet(bouquet1);
        bouquetDecoratorTulip1.setBouquet(bouquetDecoratorTulip);
        bouquetDecoratorPackaging.setBouquet(bouquetDecoratorTulip1);
        bouquetDecoratorDelivery.setBouquet(bouquetDecoratorPackaging);

        BouquetDecorator fullBouquet = bouquetDecoratorDelivery;
        System.out.println("Details : " + fullBouquet.getName());
        System.out.println("Full price : "+ fullBouquet.getPrice());
        List<String> flowers = fullBouquet.getFlowers();
        System.out.print("Flowers : ");
        for (String  s : flowers) {
            System.out.print(s + " ");
        }
        System.out.println();

    }
}
