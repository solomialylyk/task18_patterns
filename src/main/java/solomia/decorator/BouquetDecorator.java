package solomia.decorator;

import solomia.bouquet.Bouquet;

import java.util.List;
import java.util.Optional;

public class BouquetDecorator implements Bouquet {
    private Optional<Bouquet> bouquet;
    private double additionalPrice;
    private String toName = "";
    private String additionalFlowers;

    public void setBouquet(Bouquet outBouquet) {
        bouquet = Optional.ofNullable(outBouquet);
        if (additionalFlowers != null) {
            bouquet.orElseThrow(IllegalArgumentException::new).getFlowers().add(additionalFlowers);
        }
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public void setAdditionalFlowers(String additionalFlowers) {
        this.additionalFlowers = additionalFlowers;
    }

    @Override
    public List<String> getFlowers() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getFlowers();
    }

    @Override
    public double getPrice() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getPrice()+ additionalPrice;
    }

    @Override
    public String getName() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getName()+ toName;
    }
}
