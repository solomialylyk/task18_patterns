package solomia.disount;

import solomia.decorator.BouquetDecorator;

public class Discount extends BouquetDecorator {
    private  final double ADDITIONAL_PRICE = 10;
    private  final String TO_NAME = "Discount -10";

    public Discount() {
        setAdditionalFlowers(TO_NAME);
        setAdditionalPrice(-ADDITIONAL_PRICE);
    }
}
